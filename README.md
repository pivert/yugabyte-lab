# yugabyte-lab

This repository provides the scripts to deploy a YugabyteDB lab on Ubuntu 22.04 in minutes.

The scripts are strongly inspired from the YugabyteDB installation manual (3 nodes)

[Three+ data center (3DC)](https://docs.yugabyte.com/preview/deploy/multi-dc/3dc-deployment/)

## Comments:
- The documentation applies to AWS, but the scripts are adapting configuration to run on 3 local servers.
- The YugabyteDB 3 nodes cluster will be installed as root (can be easily adapted in the ansible playbook if needed)
- By default, authentication is disabled on YugabyteDB

## Content of the folder:

1. 
2.
